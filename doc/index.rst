..
    :copyright: Copyright (c) 2014 ftrack

#################
ftrack Python API
#################

Welcome to the ftrack :term:`Python` :term:`API` documentation.

.. important::

    This is the new :term:`Python` client for the ftrack :term:`API`. It can be
    considered relatively stable and ready for production use though there may
    be missing features and a few bugs left to deal with as it approaches the
    1.0.0 release. If you are migrating from the old client then please read
    the dedicated :ref:`migration guide <release/migrating_from_old_api>`.

.. toctree::
    :maxdepth: 1

    introduction
    installing
    tutorial
    understanding_sessions
    working_with_entities
    querying
    handling_events
    caching
    locations/index
    example/index
    api_reference/index
    configuring_plugins
    event_list
    environment_variables
    security_and_authentication
    release/index
    glossary

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
